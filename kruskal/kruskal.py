from typing import Dict, List, Union, Tuple
from display import display_graph


def get_input_file(file_name: str, sort: bool = False) -> Dict[str, Union[str, List[List[str]]]]:
    """
    Open the input file given and return a dictionary containing the number of Point (N) and the list of connection.

    :param file_name: the path of the input file
    :param sort: specify if the list must be sorted or not
    :return: The dictionary with 'nb_sommet' for the number of points and 'points' for the list of connection
    """
    input_from_file: Dict[str, Union[str, List[List[str]]]] = {}
    with open(file_name) as file:
        input_from_file["nb_sommet"] = file.readline().removesuffix("\n")
        input_from_file["points"] = []
        for line in file:
            line = line.removesuffix("\n")
            input_from_file["points"].append(line.split(" "))

    # Sort the list of points if asked
    if sort:
        input_from_file["points"] = sorted(input_from_file["points"], key=lambda x: x[2])
    return input_from_file


def get_liaison(point: str, list_arete: List[List[str]], origine=None) -> List[str]:
    """
    Get the list of all possible liaison from the actual tree of a specific point, ignore the connection with it's origine.

    :param point: The actual point to check the connection
    :param list_arete: The list of connection already placed
    :param origine: The origine point from we arrived, used to not make a loop and not go backward
    :return: The list of points connected to the given point
    """
    list_point_lie: List[str] = []
    for arete in list_arete:
        if point == arete[0] and arete[1] != origine:
            list_point_lie.append(arete[1])
        elif point == arete[1] and arete[0] != origine:
            list_point_lie.append(arete[0])
    return list_point_lie


def get_starting_point(x: str, y: str, graphic_representation: List[List[str]]) -> Tuple[str, str]:
    """
    Get the first point found in the list of connection to start the process of checking for loop.

    :param x: The first point from the connection (X, Y)
    :param y: The second point from the connection (X, Y)
    :param graphic_representation: The list of connection already placed
    :return: A tuple containing the starting point of the search process and the end_point of the search.
     The end_point is the point to found to prove that this connection create a loop.
    """
    for element in graphic_representation:
        if x in element:
            return x, y
        elif y in element:
            return y, x


def recursive_tree_search(liaisons: List[str], end_point: str, actual_point: str,
                          graphic_representation: List[List[str]]) -> bool:
    """
    Recursive function to explore a tree of multiple branch to find a specific point (end_point)

    :param liaisons: All the possible connection at the actual_point
    :param end_point: The end_point to find to prove that it will create a loop
    :param actual_point: The actual point that we are parsing
    :param graphic_representation: The global representation of the connection, used to look for the possible connection
    :return: a Boolean with True as "The end_point has been found, It's a loop" and False as "Not found, we continue to parse the tree"
    """
    # If there isn't any possible liaison, we return False
    if not liaisons:
        return False
    # Else if the end point is found in the liaison, we return True
    elif end_point in liaisons:
        return True

    # For each connection in this actual point, recursion to found the end_point
    for liaison in liaisons:
        result: bool = recursive_tree_search(
            liaisons=get_liaison(point=liaison, origine=actual_point, list_arete=graphic_representation),
            end_point=end_point,
            actual_point=liaison,
            graphic_representation=graphic_representation
        )
        # If the recursive search have found the end_point, we stop the program
        if result:
            return True
    # If we explored all the possibilities and haven't found the end_point, we return False
    return False


def kruskal(file_name: str) -> List[List[str]]:
    """
    Create the minimum tree of a list of connection using the algorithm of Kruskal.

    :param file_name: The input file name with all the information
    :return: A list of list containing the connections kept
    """
    file_input: Dict[str, Union[str, List[List[str]]]] = get_input_file(file_name=file_name, sort=True)
    number_of_points: int = int(file_input["nb_sommet"])
    list_arete: List[List[str]] = file_input["points"]

    graphic_representation: List[List[str]] = []
    for arete in list_arete:
        # If the size of the min tree is equal or higher than the number of points - 1, we stop the program
        if len(graphic_representation) >= number_of_points - 1:
            break

        # We get the x and y points
        x, y = arete[0], arete[1]

        # We check if at any of the points are in the list. If not, we can safely add them to the min tree
        x_not_in_list = True
        y_not_in_list = True
        for pair in graphic_representation:
            if x in pair:
                x_not_in_list = False
            elif y in pair:
                y_not_in_list = False
        if x_not_in_list or y_not_in_list:
            graphic_representation.append(arete)
            continue

        # We now brows threw the tree to look for a potential loop
        # To do that, we start from either the x or y points with the get_starting_point function
        # Then we check if it is possible to found the end_point from the starting_point
        starting_point, end_point = get_starting_point(x, y, graphic_representation)
        liaisons = get_liaison(starting_point, graphic_representation)
        is_a_loop = recursive_tree_search(liaisons=liaisons, end_point=end_point, actual_point=starting_point,
                                          graphic_representation=graphic_representation)
        if not is_a_loop:
            graphic_representation.append(arete)

    return graphic_representation


list_connection = kruskal(file_name="../input.txt")

# Graphical representation
display_graph(list_connection)
