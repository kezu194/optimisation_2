import networkx as nx
import matplotlib.pyplot as plt


def display_graph(list_connection):
    G = nx.Graph()
    for connection in list_connection:
        G.add_edge(connection[0], connection[1], weight=int(connection[2]))

    options = {
        "font_size": 16,
        "node_size": 500,
        "node_color": "white",
        "edgecolors": "black",
        "linewidths": 3,
        "width": 3,
    }
    nx.draw_networkx(G, **options)
    # Set margins for the axes so that nodes aren't clipped
    ax = plt.gca()
    ax.margins(0.20)
    plt.axis("off")
    plt.show()
