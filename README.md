# Optimisation

## Algorithme de recherche d'arbres couvrants minimaux

**Réalisation de l'algorithme de Prim et de Kruskal**

### Valeur d'entrée :

Les valeurs en entrés sont donnée sous la forme d'un document texte tel que :
```
Nombre de sommets
extrémité1 extrémité11 poids1
extrémité2 extrémité22 poids2
... .... ...
extrémitém extrémitémm poidsm
```

Voici par exemple un exemple de fichier et sa représentation graphique :

![img.png](img/img.png)

Pour lire le fichier donnée en argument, nous utilisons cette fonction :
```python
def get_input_file(file_name: str, sort: bool = False) -> Dict[str, Union[str, List[List[str]]]]:
    """
    Open the input file given and return a dictionary containing the number of Point (N) and the list of connection.

    :param file_name: the path of the input file
    :param sort: specify if the list must be sorted or not
    :return: The dictionary with 'nb_sommet' for the number of points and 'points' for the list of connection
    """
    input_from_file: Dict[str, Union[str, List[List[str]]]] = {}
    with open(file_name) as file:
        input_from_file["nb_sommet"] = file.readline().removesuffix("\n")
        input_from_file["points"] = []
        for line in file:
            line = line.removesuffix("\n")
            input_from_file["points"].append(line.split(" "))

    # Sort the list of points if asked
    if sort:
        input_from_file["points"] = sorted(input_from_file["points"], key=lambda x: x[2])
    return input_from_file
```
Nous récupérons ainsi un dictionnaire contenant le nombre de points au total sur le graphique ainsi que la liste de
toutes les arêtes sur le graphique avec leur poids. 

### Kruskal

Dans notre cas, nous avons besoins que la liste des arêtes soient triés par ordre croissant en se basant sur leurs poids.
L'algorithme de Kruskal peut se résumer à prendre chaque arête de la liste (trié) et de vérifier si une boucle pouvait
se créer si elle était ajouté à l'arbre couvrant minimal. Si une boucle ne se crée pas, nous l'ajoutons à la liste,
sinon nous passons à l'arête suivante.

L'algorithme final ressemble à ceci :
```python
def kruskal(file_name: str) -> List[List[str]]:
    """
    Create the minimum tree of a list of connection using the algorithm of Kruskal.

    :param file_name: The input file name with all the information
    :return: A list of list containing the connections kept
    """
    file_input: Dict[str, Union[str, List[List[str]]]] = get_input_file(file_name=file_name, sort=True)
    number_of_points: int = int(file_input["nb_sommet"])
    list_arete: List[List[str]] = file_input["points"]

    graphic_representation: List[List[str]] = []
    for arete in list_arete:
        # If the size of the min tree is equal or higher than the number of points - 1, we stop the program
        if len(graphic_representation) >= number_of_points - 1:
            break

        # We get the x and y points
        x, y = arete[0], arete[1]

        # We check if at any of the points are in the list. If not, we can safely add them to the min tree
        x_not_in_list = True
        y_not_in_list = True
        for pair in graphic_representation:
            if x in pair:
                x_not_in_list = False
            elif y in pair:
                y_not_in_list = False
        if x_not_in_list or y_not_in_list:
            graphic_representation.append(arete)
            continue

        # We now brows threw the tree to look for a potential loop
        # To do that, we start from either the x or y points with the get_starting_point function
        # Then we check if it is possible to found the end_point from the starting_point
        starting_point, end_point = get_starting_point(x, y, graphic_representation)
        liaisons = get_liaison(starting_point, graphic_representation)
        is_a_loop = recursive_tree_search(liaisons=liaisons, end_point=end_point, actual_point=starting_point,
                                          graphic_representation=graphic_representation)
        if not is_a_loop:
            graphic_representation.append(arete)

    return graphic_representation
```

Pour savoir si une boucle pouvait se créer à l'ajout d'une arête, j'ai utilisé une fonction récursive pour l'exploration
d'arbre. À chaque itération d'arête, je prends un point de départ et un point d'arrivée, ces points correspondent aux
points de l'arête. En partant de l'un de ces points (s'ils sont présents sur l'arbre minimal), je parcours toutes les
arêtes connectées et vérifie si l'on trouve le point d'arrivée. Je m'assure que l'on évite d'explorer un point déjà 
examiné pour éviter de créer des boucles infinies. 

Enfin si le point d'arrivée est trouvé, on en conclu qu'il existe un
chemin permettant de se rendre d'un point à l'autre et donc que l'ajout de la nouvelle arête créera une boucle.

```python
def get_liaison(point: str, list_arete: List[List[str]], origine=None) -> List[str]:
    """
    Get the list of all possible liaison from the actual tree of a specific point, ignore the connection with it's origine.

    :param point: The actual point to check the connection
    :param list_arete: The list of connection already placed
    :param origine: The origine point from we arrived, used to not make a loop and not go backward
    :return: The list of points connected to the given point
    """
    list_point_lie: List[str] = []
    for arete in list_arete:
        if point == arete[0] and arete[1] != origine:
            list_point_lie.append(arete[1])
        elif point == arete[1] and arete[0] != origine:
            list_point_lie.append(arete[0])
    return list_point_lie


def get_starting_point(x: str, y: str, graphic_representation: List[List[str]]) -> Tuple[str, str]:
    """
    Get the first point found in the list of connection to start the process of checking for loop.

    :param x: The first point from the connection (X, Y)
    :param y: The second point from the connection (X, Y)
    :param graphic_representation: The list of connection already placed
    :return: A tuple containing the starting point of the search process and the end_point of the search.
     The end_point is the point to found to prove that this connection create a loop.
    """
    for element in graphic_representation:
        if x in element:
            return x, y
        elif y in element:
            return y, x


def recursive_tree_search(liaisons: List[str], end_point: str, actual_point: str,
                          graphic_representation: List[List[str]]) -> bool:
    """
    Recursive function to explore a tree of multiple branch to find a specific point (end_point)

    :param liaisons: All the possible connection at the actual_point
    :param end_point: The end_point to find to prove that it will create a loop
    :param actual_point: The actual point that we are parsing
    :param graphic_representation: The global representation of the connection, used to look for the possible connection
    :return: a Boolean with True as "The end_point has been found, It's a loop" and False as "Not found, we continue to parse the tree"
    """
    # If there isn't any possible liaison, we return False
    if not liaisons:
        return False
    # Else if the end point is found in the liaison, we return True
    elif end_point in liaisons:
        return True

    # For each connection in this actual point, recursion to found the end_point
    for liaison in liaisons:
        result: bool = recursive_tree_search(
            liaisons=get_liaison(point=liaison, origine=actual_point, list_arete=graphic_representation),
            end_point=end_point,
            actual_point=liaison,
            graphic_representation=graphic_representation
        )
        # If the recursive search have found the end_point, we stop the program
        if result:
            return True
    # If we explored all the possibilities and haven't found the end_point, we return False
    return False
```

### Prim

L'algorithme de Prim ne nécessite pas une liste des arêtes triée. Il consiste à prendre un premier point au hasard et
d'en sélectionner l'arête minimum. Nous ajoutons donc à notre arbre couvrant les deux points de l'arête choisit.
Nous prenons ensuite l'arête minimum parmi toutes les arêtes connectées à l'un de nos points placés dans notre arbre
minimal jusqu'à ce que tous les points soient placé sur l'arbre minimum.

Le programme pour cet algorithme ressemble en certain point au programme de Kruskal (recherche des arêtes connectées), 
il ressemble à ceci :
```python
from kruskal.kruskal import get_input_file


def get_liaison(list_point: List[str], list_arete: List[List[str]]) -> List[List[str]]:
    """
    Get all the possible liaison to used based on the already used points.

    :param list_point: List of points already placed
    :param list_arete: List of all connection
    :return: A list of possible connection to use
    """
    list_point_lie: List[List[str]] = []
    for arete in list_arete:
        for point in list_point:
            if point == arete[0] and arete[1] not in list_point:
                list_point_lie.append(arete)
            elif point == arete[1] and arete[0] not in list_point:
                list_point_lie.append(arete)
    return list_point_lie


def get_min_connection(liaisons: List[List[str]]) -> List[str]:
    """
    Get the connection with the lowest value from the list given.

    :param liaisons: List of possible connection
    :return: The connection with the min value
    """
    min_value: List[str] = liaisons[0]
    for liaison in liaisons:
        if int(liaison[2]) <= int(min_value[2]):
            min_value = liaison
    return min_value


def search_and_add(point_placed: List[str], list_arete: List[List[str]], min_tree: List[List[str]]):
    """
    Search the lowest connection value and add it to the list of connection selected.

    :param point_placed: List of point already placed
    :param list_arete: List of all connection from the file
    :param min_tree: List of connection established as the minimums
    :return: An updated list of min connection and point placed after one iteration
    """
    liaisons: List[List[str]] = get_liaison(point_placed, list_arete)
    min_value: List[str] = get_min_connection(liaisons)
    if min_value[0] not in point_placed:
        point_placed.append(min_value[0])
    else:
        point_placed.append(min_value[1])
    min_tree.append(min_value)
    return point_placed, min_tree


def prim(file_name: str) -> List[List[str]]:
    """
    Realise the Prim algorithm to found the min tree of a graph.

    :param file_name: file name with the inputs
    :return: The list of connection for the min tree
    """
    input_file: Dict[str, Union[str, List[List[str]]]] = get_input_file(file_name)
    number_of_points: int = int(input_file["nb_sommet"])
    list_arete: List[List[str]] = input_file["points"]

    # We take a random point from the list of connection
    point_placed: List[str] = [list_arete[0][0]]
    min_tree: List[List[str]] = []

    # We then start to iterate until all point have been placed
    while len(point_placed) < number_of_points:
        point_placed, min_tree = search_and_add(point_placed=point_placed, list_arete=list_arete, min_tree=min_tree)
    return min_tree
```

## Représentation graphique

Pour représenter graphiquement l'arbre couvrant minimal, j'ai utilisé les paquets _**networkx**_ et _**matplotlib**_.
```python
import networkx as nx
import matplotlib.pyplot as plt


def display_graph(list_connection):
    G = nx.Graph()
    for connection in list_connection:
        G.add_edge(connection[0], connection[1], weight=int(connection[2]))

    options = {
        "font_size": 16,
        "node_size": 500,
        "node_color": "white",
        "edgecolors": "black",
        "linewidths": 3,
        "width": 3,
    }
    nx.draw_networkx(G, **options)
    # Set margins for the axes so that nodes aren't clipped
    ax = plt.gca()
    ax.margins(0.20)
    plt.axis("off")
    plt.show()
```

En utilisant cette fonction avec les résultats du programme de Prim et de Kruskal avec ce fichier d'input, nous
obtenons le résultat suivant :

```text
6
A E 6
C D 1
B C 2
C F 4
A D 3
A B 4
B D 1
D F 4
F E 1
C E 5
```

![img.png](img/arbre.png)
