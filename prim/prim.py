from typing import List, Dict, Union

from display import display_graph
from kruskal.kruskal import get_input_file


def get_liaison(list_point: List[str], list_arete: List[List[str]]) -> List[List[str]]:
    """
    Get all the possible liaison to used based on the already used points.

    :param list_point: List of points already placed
    :param list_arete: List of all connection
    :return: A list of possible connection to use
    """
    list_point_lie: List[List[str]] = []
    for arete in list_arete:
        for point in list_point:
            if point == arete[0] and arete[1] not in list_point:
                list_point_lie.append(arete)
            elif point == arete[1] and arete[0] not in list_point:
                list_point_lie.append(arete)
    return list_point_lie


def get_min_connection(liaisons: List[List[str]]) -> List[str]:
    """
    Get the connection with the lowest value from the list given.

    :param liaisons: List of possible connection
    :return: The connection with the min value
    """
    min_value: List[str] = liaisons[0]
    for liaison in liaisons:
        if int(liaison[2]) <= int(min_value[2]):
            min_value = liaison
    return min_value


def search_and_add(point_placed: List[str], list_arete: List[List[str]], min_tree: List[List[str]]):
    """
    Search the lowest connection value and add it to the list of connection selected.

    :param point_placed: List of point already placed
    :param list_arete: List of all connection from the file
    :param min_tree: List of connection established as the minimums
    :return: An updated list of min connection and point placed after one iteration
    """
    liaisons: List[List[str]] = get_liaison(point_placed, list_arete)
    min_value: List[str] = get_min_connection(liaisons)
    if min_value[0] not in point_placed:
        point_placed.append(min_value[0])
    else:
        point_placed.append(min_value[1])
    min_tree.append(min_value)
    return point_placed, min_tree


def prim(file_name: str) -> List[List[str]]:
    """
    Realise the Prim algorithm to found the min tree of a graph.

    :param file_name: file name with the inputs
    :return: The list of connection for the min tree
    """
    input_file: Dict[str, Union[str, List[List[str]]]] = get_input_file(file_name)
    number_of_points: int = int(input_file["nb_sommet"])
    list_arete: List[List[str]] = input_file["points"]

    # We take a random point from the list of connection
    point_placed: List[str] = [list_arete[0][0]]
    min_tree: List[List[str]] = []

    # We then start to iterate until all point have been placed
    while len(point_placed) < number_of_points:
        point_placed, min_tree = search_and_add(point_placed=point_placed, list_arete=list_arete, min_tree=min_tree)
    return min_tree


list_connection = prim(file_name="../input.txt")

# Graphical representation
display_graph(list_connection)
